var
  env        	         = require('minimist')(process.argv.slice(2)),
  gulp       	         = require('gulp'),
  uglify     	         = require('gulp-uglify'),
  concat     	         = require('gulp-concat'),
  gulpif     	         = require('gulp-if'),
  connect    	         = require('gulp-connect'),
  modRewrite 	         = require('connect-modrewrite'),
  imagemin   	         = require('gulp-imagemin'),
  include              = require("gulp-include"),
  del 				         = require('del'),
  less				         = require('gulp-less'),
  ttf2woff             = require('gulp-ttf2woff'),
  consolidate          = require('gulp-consolidate'),
  iconfontCss          = require('gulp-iconfont-css'),
  iconfont             = require('gulp-iconfont'),
  LessPluginCleanCSS   = require('less-plugin-clean-css'),
  LessPluginAutoPrefix = require('less-plugin-autoprefix'),
  rename               = require('gulp-rename'),
  autoprefixer         = require('gulp-autoprefixer'),
  maps                 = require('gulp-sourcemaps'),
  browserSync          = require('browser-sync').create(),
  sass                 = require('gulp-sass'),
  cssmin               = require('gulp-cssmin');

var
  srcDir 		= 'src/',
  buildDir	= 'build/',
  assetsDir = buildDir + 'assets/',
  cleancss = new LessPluginCleanCSS({
    advanced: true
  }),
  autoprefix = new LessPluginAutoPrefix({
    browsers: ["last 2 versions"]
  });

gulp.task('less', function(){
  return gulp.src(srcDir + 'less/main.less')
    .pipe(less({
      plugins: [autoprefix, cleancss]
    }))
    .on('error', function(error){
      console.log(error.toString());
      this.emit('end');
    })
    .pipe(gulp.dest(assetsDir + 'css'))
    .pipe(connect.reload());
});

gulp.task('compileSass', function() {
  return gulp.src(srcDir + 'sass/main.scss')
    .pipe(maps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(maps.write('./'))
    .pipe(gulp.dest(assetsDir + 'css'))
    .pipe(browserSync.stream());
});

gulp.task("minifyCss", ["compileSass"], function() {
  return gulp.src(assetsDir + 'css')
    .pipe(cssmin())
    .pipe(rename('main.min.css'))
});

gulp.task('js', function(){
  return gulp.src(srcDir + 'js/**/*.js')
    .pipe(concat('main.js'))
    .pipe(include())
    .on('error', console.log)
    .pipe(gulpif(env.p, uglify()))
    .pipe(gulp.dest(assetsDir + 'js/'))
    .pipe(connect.reload());
});

gulp.task('img', function() {
  return gulp.src(srcDir + 'img/**/*')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest(assetsDir + 'img'))
    .pipe(connect.reload());
});

gulp.task('html', function() {
  gulp.src([srcDir + 'html/*.html'])
    .pipe(include())
    .on('error', console.log)
    .pipe(gulp.dest('./' + buildDir))
    .pipe(connect.reload());
});

gulp.task('fonts', function(){
  gulp.src([srcDir + 'fonts/**/*'])
    .pipe(ttf2woff())
    .pipe(gulp.dest(assetsDir + 'fonts/'))
    .pipe(connect.reload());
});

gulp.task('fontsSass', function(){
  gulp.src([srcDir + 'sass/fonts/*.{eot,woff2,woff,ttf,svg}'])
    .pipe(ttf2woff())
    .pipe(gulp.dest(assetsDir + 'css/fonts/'))
    .pipe(connect.reload())
});

gulp.task('imgSass', function() {
  return gulp.src(srcDir + 'sass/*.{gif}')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest(assetsDir + 'css'))
    .pipe(connect.reload());
});

gulp.task('iconfont', function () {
  return gulp.src(srcDir + 'img/svg/*.svg')
    .pipe(iconfontCss({
      fontName: 'iconfont',
      targetPath: '../icons.scss',
      fontPath: 'fonts/'
    }))
    .pipe(iconfont({
      fontName: 'iconfont',
      formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'],
      appendCodepoints: true,
      appendUnicode: false,
      normalize: true,
      fontHeight: 1000,
      centerHorizontally: true
    }))
    .pipe(gulp.dest(srcDir + 'sass/fonts/'))
    .pipe(connect.reload());
});

gulp.task('clean', function() {
  return del([
    buildDir + '**/*'
  ]);
});

// Call Watch
gulp.task('monitor', function(){
  gulp.watch(srcDir + 'sass/*.scss', ['minifyCss']);
  gulp.watch(srcDir + 'sass/**/*.{gif}', ['imgSass']);
  gulp.watch(srcDir + 'sass/fonts/**/*.{eot,woff2,woff,ttf,svg}', ['fontsSass']);
  gulp.watch(srcDir + 'js/**/*.js', ['js']);
  gulp.watch(srcDir + 'html/**/*.html', ['html']);
  gulp.watch(srcDir + 'img/**/*.{jpg,png,gif}', ['img']);
  gulp.watch(srcDir + 'img/svg/*.svg', ['iconfont']);
  gulp.watch(srcDir + 'fonts/**/*.{eot,woff2,woff,ttf,svg}', ['fonts']);
});

// Connect (Livereload)
gulp.task('connect', function() {
  connect.server({
    root: [buildDir],
    livereload: true,
    middleware: function(){
      return [
        modRewrite([
          '^/$ /index.html',
          '^([^\\.]+)$ $1.html'
        ])
      ];
    }
  });
});

// Default task
gulp.task('watch', ['default', 'connect', 'monitor']);

// Default task
gulp.task('default', ['minifyCss', 'js', 'img', 'html', 'iconfont', 'fonts', 'fontsSass', 'imgSass']);
